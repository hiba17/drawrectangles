# DrawRectangles


## General Info
This is a simple system that draws rectangles using #. You as the user decides te width and the height of the rectangle.


## Technologies
- Visual Studio Code
- Java SE

## Prerequesites
Install java using this link:
https://www.oracle.com/java/technologies/javase-downloads.html

Install VS code using this link:

https://code.visualstudio.com/


## Features
1. Creates a rectangle with the widt and height given from user
2. HAs added functionality that creates a small innerrectangle inside the outer one that has single character space,
if the outer rectangle has enough room.

## Difficulties
I manually coded in the inner rectangle but there is probably an easier way to solve that task
