import java.util.*;

public class Rectangle{

    //method does a try catch to check if input is whole number, return boolean if wrong
    static boolean numberOrNot(String number){
        try {
             Integer.parseInt(number);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
    //method that keeps asking for input from user if number is invalid, return valid number
    static int checkInput(Scanner input){
        String number = input.next();
        while(numberOrNot(number) == false || Integer.parseInt(number)<0){
            System.out.println("Thats is not a whole positive number, try again:\n"); 
            number = input.next(); 
        }
        return Integer.parseInt(number);
    }
    
    static void createRectangle(int height, int width){
         //removing the outer rectangle and a single character space on each side
         //giving the inner rectangles total width + height
         int innerHeight = height-4;
         int innerWidth = width-4;
        //end of the row/column
         int outerHeight = height-1;
         int outerWidth = width-1;
         
         //checking if there are possible to draw a second rectangle inside the first
         //that means that the inner-height and width must be bigger or equal to 2 
         //smaller than 2  means that there is no room 
         if((innerHeight >= 2) && (innerWidth >= 2)){
             for(int i=0; i<width; i++){
                 for(int j=0; j < height; j++){
                     if(i==0 || i==outerWidth || j==0 || j==outerHeight){        
                         System.out.print("#");
                     } 
                     //print # only when row or column is 2 or the end of the inner-height/width
                     else if( (i==2 && j>=2 && j<(height-2))  || (j==2 && i>=2 && i<(width-2) ) || i>2 && i<(width-3) && j==(height-3)|| i==(width-3) && j>2 && j<(height-2) ) {
                         System.out.print("#");
                     }
                     //if none of the conditions above, print space  
                     else{
                         System.out.print(" ");
                     }    
                 }
                 //new line after every row
                 System.out.print("\n");
             }
         }
         //if there is no possibility in drawing a inner rectangle , just draw the outer one
         //same format as above
         else{
             for(int i=0; i< width;i++){
                 for(int j=0; j < height; j++){
                     if(i==0 || i==outerWidth || j==0 || j==outerHeight){        
                         System.out.print("#");
                     } 
                     else{
                         System.out.print(" ");
                     }    
                 }
                 System.out.print("\n");
             }
         }
    }
    static void mainMethod(Scanner input){
        
         //Gets width and height from user and used method CheckInput to check if it is a whole positive number
         System.out.println("How large do you want the width of the rectangle to be?\n"); 
         int width = checkInput(input);
       
         System.out.println("How large do you want the height of the rectangle to be?\n");
         int height = checkInput(input);
 
         //check for if height equals width, iif it does, we run checkInput again and ask for new height
         while(height==width){
             System.out.println("Your height and width are equal. " +
             "How large do you want the height of the rectangle to be?\n");
              height = checkInput(input);
         }
         
         
         System.out.println("Here it is: ");
         //take the inputs and do a double for loop and print out #,with an optional upgrade
         createRectangle(height,width);
        
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //Start message
        
        System.out.println("__WELCOME TO DRAW ME A RECTANGLE__"+"\n"+"__The best game in town!__\n\n");
        System.out.println("Do you want to:\n - start Game (Press s)\n -quit (press q)"); 
        String sq = input.next();

        //as long as user hasn't pushed q
        while(!sq.equals("q")){
            //as long as user doesn't write s, user wrote something wrong,try again
            if(!sq.equals("s")){
                System.out.println("Upps, you wrote something wrong!\n Try Again\n - start Game (Press s)\n -quit (press q)"); 
                sq = input.next();
            }
            //if not then everything is okey, go to mainMathod
            else{               
                mainMethod(input);
                System.out.println("\n - start Game again (Press s)\n -quit (press q)"); 
                sq = input.next();
            }
        }

        //end of game
        System.out.println("\n Thanks for playing! See you soon :) ");   
    }

     

    
}

